## CHANGELOG

### v0.4.1
- Swapped Accessibility and About NYPL links

### v0.4.0
- Updated to React 15.
