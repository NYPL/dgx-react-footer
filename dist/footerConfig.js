'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
var data = {
  nyplLinks: [[{ name: 'Accessibility',
    link: '/accessibility'
  }, { name: 'Press',
    link: '/help/about-nypl/media-center'
  }, { name: 'Careers',
    link: '/careers'
  }, { name: 'Space Rental',
    link: '/spacerental'
  }], [{ name: 'Privacy Policy',
    link: '/help/about-nypl/legal-notices/privacy-policy'
  }, { name: 'Other Policies',
    link: '/policies'
  }, { name: 'Terms & Conditions',
    link: '/terms-conditions'
  }, { name: 'Governance',
    link: '/help/about-nypl/leadership/board-trustees'
  }], [{ name: 'Rules & Regulations',
    link: '/help/about-nypl/legal-notices/rules-and-regulations'
  }, { name: 'About NYPL',
    link: '/help/about-nypl'
  }, { name: 'Language',
    link: '/language'
  }]],
  socialMedia: [{ name: 'NYPL on Facebook',
    link: 'https://www.facebook.com/nypl',
    className: 'nypl-icon-facebook'
  }, { name: 'NYPL on Twitter',
    link: 'https://twitter.com/nypl',
    className: 'nypl-icon-twitter'
  }, { name: 'NYPL on Instagram',
    link: 'https://instagram.com/nypl',
    className: 'nypl-icon-instagram'
  }, { name: 'NYPL on Tumblr',
    link: 'https://nypl.tumblr.com/',
    className: 'nypl-icon-tumblr'
  }, { name: 'NYPL on Youtube',
    link: 'https://www.youtube.com/user/NewYorkPublicLibrary',
    className: 'nypl-icon-youtube'
  }]
};

exports.default = data;